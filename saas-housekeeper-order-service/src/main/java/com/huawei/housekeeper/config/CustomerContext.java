
package com.huawei.housekeeper.config;

import com.huawei.housekeeper.model.Customer;

/**
 * 用户上下文
 *
 * @author lWX1128557
 * @since 2022-03-21
 */
public class CustomerContext {
    private static final ThreadLocal<Customer> CUSTOMER = new ThreadLocal<>();

    /**
     * 保存租户信息
     *
     * @param customer 租户
     */
    public static void setCustomer(Customer customer) {
        CUSTOMER.set(customer);
    }

    /**
     * 获取租户信息
     *
     * @return 租户
     */
    public static Customer getCustomer() {
        return CUSTOMER.get();
    }

    /**
     * 移除租户信息
     */
    public static void removeCustomer() {
        CUSTOMER.remove();
    }

}
