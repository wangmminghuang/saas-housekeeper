
package com.huawei.housekeeper.constant;

/**
 * 任务状态常量
 *
 * @author lWX1128557
 * @since 2022-04-12
 */
public interface TaskCodeConstant {
    /**
     * 待接单
     */
    int WAITING = 1;

    /**
     * 已完成
     */
    int COMPELETED = 2;

    /**
     * 取消
     */
    int CANCEL = 3;

    /**
     * 进行中
     */
    int ONGOING = 4;
}
