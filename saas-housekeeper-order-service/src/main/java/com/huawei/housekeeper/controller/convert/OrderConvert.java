
/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.convert;

import com.huawei.housekeeper.model.ServiceDetail;
import com.huawei.housekeeper.entity.OrderMessageEntity;
import com.huawei.housekeeper.controller.response.GetOrderDetailsVo;
import com.huawei.housekeeper.controller.response.GetOrdersOfCustomerVo;
import com.huawei.housekeeper.controller.response.GetOrdersOfTenantVo;
import com.huawei.housekeeper.dao.entities.Order;

import com.alibaba.fastjson.JSONObject;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * mapstruct转换器
 *
 * @author lWX1128557
 * @since 2022-02-23
 */
@Mapper(componentModel = "spring") // 启用映射转换
public interface OrderConvert {
    /**
     * 对象映射转换实例
     */
    OrderConvert INSTANCE = Mappers.getMapper(OrderConvert.class);

    /**
     * 订单映射客户订单列表Vo
     *
     * @param orders 订单表
     * @return 用户订单列表Vo
     */
    List<GetOrdersOfCustomerVo> toGetOrdersOfCustomerVo(List<Order> orders);

    /**
     * 客户订单详情映射
     *
     * @param order         订单表
     * @param serviceDetail 服务详情
     * @return 客户订单详情Vo
     */
    @Mappings({@Mapping(target = "orderId", source = "order.id"),
            @Mapping(target = "serviceDetail", source = "serviceDetail"),
            @Mapping(target = "imgSrc", source = "serviceDetail.serviceDesc.imgSrc")})
    GetOrdersOfCustomerVo mapGetOrdersOfCustomerVo(Order order, ServiceDetail serviceDetail);

    /**
     * 客户订单详情Vo的服务规格映射
     *
     * @param serviceDetail 服务规格
     * @return 用户订单详情Vo的服务规格
     */
    @Mappings({@Mapping(target = "serviceName", source = "serviceDesc.serviceName"),
            @Mapping(target = "specifications", source = "serviceDesc.selections")})
    GetOrdersOfCustomerVo.ServiceDetail mapGetOrdersOfCustomerVoServiceDetail(ServiceDetail serviceDetail);

    /**
     * 客户订单详情Vo的服务规格详情映射
     *
     * @param selections 服务规格详情
     * @return 用户订单详情Vo的服务规格详情
     */
    List<GetOrdersOfCustomerVo.Specifications> mapGetOrdersOfCustomerVoSpecifications(List<ServiceDetail.Selections> selections);

    /**
     * 订单表映射客户订单列表Vo
     *
     * @param order 订单表
     * @return 客户订单列表Vo
     */
    default GetOrdersOfCustomerVo mapCustomerVo(Order order) {
        ServiceDetail serviceDetail = getServiceDetail(order);
        mapGetOrdersOfCustomerVoSpecifications(serviceDetail.getServiceDesc().getSelections());
        mapGetOrdersOfCustomerVoServiceDetail(serviceDetail);
        return mapGetOrdersOfCustomerVo(order, serviceDetail);
    }

    /**
     * 订单转租户订单列表Vo
     *
     * @param orders 订单表
     * @return 租户订单列表Vo
     */
    List<GetOrdersOfTenantVo> toGetOrdersOfTenantVo(List<Order> orders);

    /**
     * 租户查询订单详情映射
     *
     * @param order         订单表
     * @param serviceDetail 服务规格
     * @return 租户查询订单详情Vo
     */
    @Mappings({@Mapping(target = "orderId", source = "order.id"),
            @Mapping(target = "serviceDetail", source = "serviceDetail"),
            @Mapping(target = "imgSrc", source = "serviceDetail.serviceDesc.imgSrc")})
    GetOrdersOfTenantVo mapGetOrdersOfTenantVo(Order order, ServiceDetail serviceDetail);

    /**
     * 租户查询订单详情的服务详情映射
     *
     * @param serviceDetail 服务详情
     * @return 租户查询订单详情Vo的服务详情
     */
    @Mappings({@Mapping(target = "serviceName", source = "serviceDesc.serviceName"),
            @Mapping(target = "specifications", source = "serviceDesc.selections"),
            @Mapping(target = "price", source = "serviceDesc.price")})
    GetOrdersOfTenantVo.ServiceDetail mapGetOrdersOfTenantVoServiceDetail(ServiceDetail serviceDetail);

    /**
     * 租户查询订单详情的服务规格列表映射
     *
     * @param selections 服务规格
     * @return 租户查询订单详情Vo的服务规格列表
     */
    List<GetOrdersOfTenantVo.Specifications> mapGetOrdersOfTenantVoSpecifications(List<ServiceDetail.Selections> selections);

    /**
     * 租户查询订单详情Vo映射
     *
     * @param order 订单表
     * @return 租户订单列表Vo
     */
    default GetOrdersOfTenantVo mapTenantVo(Order order) {
        ServiceDetail serviceDetail = getServiceDetail(order);
        mapGetOrdersOfTenantVoSpecifications(serviceDetail.getServiceDesc().getSelections());
        mapGetOrdersOfTenantVoServiceDetail(serviceDetail);
        return mapGetOrdersOfTenantVo(order, serviceDetail);
    }

    /**
     * 订单详情映射
     *
     * @param order         订单表
     * @param serviceDetail 服务详情
     * @return 订单详情Vo
     */
    @Mappings({@Mapping(target = "serviceDetail", source = "serviceDetail"),
            @Mapping(target = "imgSrc", source = "serviceDetail.serviceDesc.imgSrc"),
            @Mapping(target = "orderId", source = "order.id"),
            @Mapping(target = "createTime", source = "order.createdTime")})
    GetOrderDetailsVo mapOrderDetailsVo(Order order, ServiceDetail serviceDetail);

    /**
     * 订单详情Vo的服务详情映射
     *
     * @param serviceDetail 服务详情
     * @return 订单详情Vo的服务详情
     */
    @Mappings({@Mapping(target = "serviceName", source = "serviceDesc.serviceName"),
            @Mapping(target = "specifications", source = "serviceDesc.selections")})
    GetOrderDetailsVo.ServiceDetail mapOrderDetailsVoServiceDetail(ServiceDetail serviceDetail);

    /**
     * 订单详情Vo的服务规格列表映射
     *
     * @param selections 服务规格
     * @return 订单详情Vo的服务规格列表
     */
    List<GetOrderDetailsVo.Specifications> mapSpecifications(List<ServiceDetail.Selections> selections);

    /**
     * 服务详情json封装
     *
     * @param order 订单表
     * @return 服务详情
     */
    static ServiceDetail getServiceDetail(Order order) {
        // 订单详情封装
        String serviceDetailString = order.getServiceDetail();
        return JSONObject.parseObject(serviceDetailString, ServiceDetail.class);
    }

    /**
     * 订单消息体映射
     *
     * @param order         订单表
     * @param serviceDetail 服务详情
     * @return 订单消息体
     */
    @Mappings({@Mapping(target = "orderId", source = "order.id"),
            @Mapping(target = "serviceName", source = "serviceDetail.serviceDesc.serviceName"),
            @Mapping(target = "serviceDetail", source = "serviceDetail"),
            @Mapping(target = "appointment", source = "order.appointmentTime")})
    OrderMessageEntity mapOrderMessageEntity(Order order, ServiceDetail serviceDetail);

    /**
     * 订单消息体的服务详情映射
     *
     * @param serviceDetail 服务详情
     * @return 订单消息体的服务详情
     */
    @Mappings({@Mapping(target = "selections", source = "serviceDesc.selections")})
    OrderMessageEntity.ServiceDetail mapOrderMessageEntityServiceDetail(ServiceDetail serviceDetail);

    /**
     * 订单消息体的服务规格列表映射
     *
     * @param selections 服务规格
     * @return 订单消息体的服务规格列表
     */
    List<OrderMessageEntity.Selections> mapOrderMessageEntitySelections(List<ServiceDetail.Selections> selections);

    default OrderMessageEntity.Selections mapOrderMessageEntitySelection(ServiceDetail.Selections selection) {
        OrderMessageEntity.Selections selections = new OrderMessageEntity.Selections();
        selections.setSpecName(selection.getSpecName()); // 规格名称
        selections.setSelection(selection.getOptionName()); // 规格详情
        return selections;
    }
}