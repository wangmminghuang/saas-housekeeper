
/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller;

import com.huawei.housekeeper.result.ListRes;
import com.huawei.housekeeper.result.Result;
import com.huawei.housekeeper.controller.request.CreateOrderDto;
import com.huawei.housekeeper.controller.request.PageQueryOrderDto;
import com.huawei.housekeeper.controller.request.UpdateOrderDto;
import com.huawei.housekeeper.controller.response.GetOrderDetailsVo;
import com.huawei.housekeeper.controller.response.GetOrdersOfCustomerVo;
import com.huawei.housekeeper.controller.response.GetOrdersOfTenantVo;
import com.huawei.housekeeper.service.OrderService;

import com.fasterxml.jackson.core.JsonProcessingException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * 订单controller
 *
 * @author lWX1128557
 * @since 2022-02-23
 */
@RestController
@RequestMapping("/order")
@Api(tags = "订单控制层")
@Valid
public class OrderController {
    @Autowired
    private OrderService orderService;

    @PostMapping(value = "/customer-orders")
    @ApiOperation(value = "用户查询订单列表")
    public Result<ListRes<GetOrdersOfCustomerVo>>
        getCustomerOrders(@RequestBody @Valid PageQueryOrderDto pageQueryOrderDto) {
        return Result.createResult(orderService.getOrdersByCustomer(pageQueryOrderDto));
    }

    @PostMapping(value = "/tenant-orders")
    @ApiOperation(value = "租户查询订单列表")
    public Result<ListRes<GetOrdersOfTenantVo>> getTenantOrders(@RequestBody @Valid PageQueryOrderDto pageQueryOrderDto,
        @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(orderService.getOrdersByTenant(pageQueryOrderDto));
    }

    @PostMapping(value = "/order")
    @ApiOperation(value = "用户下单")
    public Result<Long> saveOrder(@Valid @RequestBody CreateOrderDto orderDto,
        @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain)
        throws JsonProcessingException {
        return Result.createResult(orderService.saveOrder(orderDto));
    }

    @GetMapping(value = "/order/orderNumber/{orderNumber}")
    @ApiOperation(value = "根据订单号查询订单")
    public Result<GetOrderDetailsVo> getOrderByOrderNumber(
        @Length(min = 1, max = 255, message = "长度范围：1-255") @NotNull @PathVariable("orderNumber") String orderNumber) {
        return Result.createResult(orderService.getOrderByOrderNumber(orderNumber));
    }

    @GetMapping(value = "/order/orderId/{orderId}")
    @ApiOperation(value = "根据订单id查询订单")
    public Result<GetOrderDetailsVo> getOrderByOrderId(
        @Length(min = 1, max = 32, message = "长度范围：1-32") @NotNull @PathVariable("orderId") Integer orderId) {
        return Result.createResult(orderService.getOrderByOrderId(orderId));
    }

    @PutMapping(value = "/order")
    @ApiOperation(value = "根据订单编号更新订单状态")
    public Result<Integer> updateOrder(@Valid @RequestBody UpdateOrderDto updateOrderDto)
        throws JsonProcessingException {
        return Result.createResult(orderService.updateOrder(updateOrderDto));
    }
}
