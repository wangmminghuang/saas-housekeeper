/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author jWX1116205
 * @since 2022-01-05
 */
@Setter
@Getter
@ApiModel("查询服务对象")
public class GetServiceDto {

    @NotNull(message = "服务ID必填")
    @Min(value = 1L, message = "最小值：1")
    @ApiModelProperty(value = "服务ID", required = true)
    private Long serviceId;

}