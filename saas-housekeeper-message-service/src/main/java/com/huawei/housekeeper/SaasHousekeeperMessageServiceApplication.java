package com.huawei.housekeeper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 消息中心
 *
 * @since 2022-02-15
 */

@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = {"com.huawei.**"})
public class SaasHousekeeperMessageServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SaasHousekeeperMessageServiceApplication.class, args);
    }
}
