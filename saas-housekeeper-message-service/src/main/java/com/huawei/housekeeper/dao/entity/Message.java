/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.dao.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.huawei.housekeeper.entity.BaseEntity;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 功能描述
 *
 * @since 2022-02-15
 */
@Getter
@Setter
@TableName("message")
public class Message {
    /**
     * 用户id
     */
    @TableField("id")
    private String id;
    /**
     * 用户id
     */
    @TableField("user_id")
    private String userId;

    /**
     * 消息标题
     */
    @TableField("title")
    private String title;

    /**
     * 具体内容
     */
    @TableField("content")
    private String content;

    /**
     * 连接
     */
    @TableField("link")
    private String link;

    /**
     * 信息状态 0 未读,1 已读,2 删除
     */
    @TableField("message_status")
    private int messageStatus;

    /**
     * 来自
     */
    @TableField("info_from")
    private String infoFrom;

    /**
     * 创建时间
     */
    @TableField(value = "CREATED_TIME", updateStrategy = FieldStrategy.NEVER, fill = FieldFill.INSERT)
    private Date createdTime;
}
