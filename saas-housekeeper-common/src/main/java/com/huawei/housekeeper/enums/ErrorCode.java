/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.enums;

import com.huawei.housekeeper.constants.BaseCode;

import lombok.Getter;

/**
 * 状态码枚举类
 *
 * @since 2021-11-30
 */
@Getter
public enum ErrorCode implements BaseCode {

    /**
     *   10 通用
     *   11 用户
     *   12 租户
     *   13 业务
     *   14 订单
     */

    //10 通用

    FORBIDDEN(100001, "没有权限"),

    SERVER_ERROR(100002, "系统异常"),

    UNKNOWN(100003, "未知异常"),

    PARAM_INVALID(100004, "参数非法"),

    IS_EMPTY(100005, "参数为空"),

    MESSAGE_ERROR(100006, "消息接收失败"),

    QUERY_RESULT_EMPTY(100007, "查询结果为空！"),


    //11 用户
    USERNAME_OR_PASSWORD_ERROR(110001, "用户名或者密码错误"),

    USERNAME_REPEATED(110002, "用户名重复!"),

    DELETE_ERROR(110003, "不能删除当前用户"),

    USER_EMPTY(110004, "用户不存在"),

    TOKEN_EXPIRED(110006, "token过期"),

    TOKEN_ERROR(110007, "token错误"),

    //12 租户
    TENANT_REPEAT(120001, "名字/域名/企业信用码重复"),

    TENANT_ROUTING_ERROR(120002, "路由失败"),

    TENANT_MISS_ERROR(120003, "缺失租户标识!"),

    STATUS_ERROR(120004, "租户状态更改有误"),

    DATABASE_REPEAT(120005, "租户数据库重复"),

    INVALID_VERIFICATION(120006, "邮箱验证码过期"),

    VERIFICATION_INCORRECT(120007, "验证码错误"),

    REGISTRATION_FAILED(120008, "发起注册失败！"),

    EMAIL_REGISTERED(120009, "邮箱已被注册"),


    //13 业务
    SERVICE_NAME_REPEATED(130000, "服务名重复!"),

    SERVICE_NOT_EXIST(130001, "服务不存在!"),

    SPECIFICATION_REPEATED(130002, "服务规格已存在!"),

    SPECIFICATION_NOT_EXIST(1300003, "服务规格不存在!"),

    SKU_NOT_EXIST(130004, "该skuId没有对应的数据!"),

    SELECT_REPEATED(130005, "服务选集重复!"),

    SPECIFICATION_SELECT_EMPTY(130006, "规格选项为空!"),

    SERVICE_SELECT_EMPTY(130007, "服务选集为空!"),

    //14 订单
    TASK_UPDATE_NOT_PERMISSION_CANCEL(140001, "取消操作，任务的状态必须是已接单！!"),

    TASK_UPDATE_NOT_PERMISSION_FINISHED(140002, "任务完成操作，任务的状态必须是已接单！"),

    TASK_UPDATE_NOT_PERMISSION_GRAP(140003, "抢单操作，任务的状态必须是待抢单！"),

    TASK_UPDATE_NOT_PERMISSION(140004, "任务操作非法!"),

    TASK_ORDER_CANCELED(140005, "订单任务已取消!"),

    TASK_NOT_EXIST(140006, "任务不存在!"),

    TASK_ORDER_EXIST(140007, "订单任务已存在!"),

    TASK_GRAB_FAILED(140009, "抢单失败!"),

    UPLOAD_FILE_ERROR(140010,"上传失败"),

    UPLOAD_FILE_TYPE_ERROR(140011,"上传文件格式错误");


    private final int code;

    private final String message;


    ErrorCode(int code, String message) {
        this.code = code;
        this.message = message;
    }
}