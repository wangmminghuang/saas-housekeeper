package com.huawei.housekeeper.service;

import com.huawei.housekeeper.entity.TenantMqMessage;
import com.huawei.saashousekeeper.context.TenantContext;

public abstract class TenantMessageHandler<T extends TenantMqMessage> extends MessageHandler<T> {
    /**
     * 多租消息的公共处理把租户id存入租户上下文
     */
    @Override
    public void dispatchMessage(T message){
        TenantContext.setDomain(message.getTenantId(),true);
        super.dispatchMessage(message);
    }
}
