package com.huawei.housekeeper.entity;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public abstract class BaseMqMessage {
    protected String key;
    protected LocalDateTime sendTime = LocalDateTime.now();
}
