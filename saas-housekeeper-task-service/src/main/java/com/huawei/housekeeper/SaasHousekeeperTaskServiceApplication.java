/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper;

import lombok.extern.log4j.Log4j2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 任务大厅服务
 *
 * @author jWX1116205
 * @since 2022-02-15
 */
@Log4j2
@EnableFeignClients
@EnableEurekaClient
@SpringBootApplication(scanBasePackages = {"com.huawei.**"})
public class SaasHousekeeperTaskServiceApplication {
    public static void main(String[] args) {
        log.info("task info module start...");
        SpringApplication.run(SaasHousekeeperTaskServiceApplication.class, args);
        log.info("task info module start over...");

    }
}