/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.config.binding;

/**
 * schema转换器
 *
 * @author lWX1156935
 * @since 2022/4/22
 */
public interface SchemaBindingStrategy {
    /**
     * key转换成schema,获取schema
     *
     * @param key 关键字
     * @return schema
     */
    String getSchema(String key);
}
