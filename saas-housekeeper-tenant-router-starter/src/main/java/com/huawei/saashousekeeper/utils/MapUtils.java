/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.utils;

import java.util.Map;

/**
 * map工具类
 *
 * @author lWX1156935
 * @since 2022-06-22
 */
public class MapUtils {
    /**
     * 是否为空
     *
     * @param map map
     * @return 结果
     */
    public static boolean isEmpty(Map map) {
        return map == null || map.isEmpty();
    }

    /**
     * map属性合并，target覆盖source
     *
     * @param target 目标
     * @param source 数据源
     * @return 合并map
     */
    public static Map mergeIn(Map<String, Object> target, Map<String, Object> source) {
        if (target == null) {
            return source;
        }
        if (source == null) {
            return target;
        }
        source.putAll(target);
        return source;
    }
}
