/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.exception;

/**
 * 关闭异常
 *
 * @author lWX1156935
 * @since 2022-07-13
 */
public class CloseNotSupportException extends RuntimeException {
    private static final long serialVersionUID = -3010107192188843973L;

    private int code;

    /**
     * 构造器
     *
     * @param code 异常码
     * @param message 异常信息
     * @param cause cause
     */
    public CloseNotSupportException(int code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    /**
     * 异常
     *
     * @param message 信息
     */
    public CloseNotSupportException(String message) {
        super(message);
    }

    /**
     * 异常
     *
     * @param code 异常码
     * @param message 信息
     */
    public CloseNotSupportException(int code, String message) {
        this(code, message, null);
    }
}
