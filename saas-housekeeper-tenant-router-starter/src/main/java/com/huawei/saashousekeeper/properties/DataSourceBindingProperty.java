/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.properties;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 绑定关系
 *
 * @author lWX1156935
 * @since 2022/4/22
 */
@Data
@Accessors(chain = true)
public class DataSourceBindingProperty {
    private String groupName;

    private String schema;
}
