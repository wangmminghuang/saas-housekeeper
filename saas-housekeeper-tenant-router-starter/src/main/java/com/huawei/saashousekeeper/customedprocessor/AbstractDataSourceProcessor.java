/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.customedprocessor;

import com.huawei.saashousekeeper.dbpool.PoolStrategy;
import com.huawei.saashousekeeper.properties.DataSourceProperty;

import org.springframework.core.Ordered;

import javax.sql.DataSource;

/**
 * PoolStrategy.getPoolName() 为空时，所有类型连接池都会处理，不为空时，只会针对相应的连接池类型才生效
 * 后续看需求再添加其他生效策略
 *
 * @author lWX1156935
 * @since 2022-07-07
 */
public abstract class AbstractDataSourceProcessor implements Ordered, PoolStrategy {

    /**
     * 处理器前置操作
     *
     * @param property 连接属性
     */
    public abstract void beforeCreate(DataSourceProperty property);

    /**
     * 处理器后置操作
     *
     * @param property 连接属性
     * @param dataSource 数据源
     */
    public abstract DataSource afterCreate(DataSourceProperty property, DataSource dataSource);
}
