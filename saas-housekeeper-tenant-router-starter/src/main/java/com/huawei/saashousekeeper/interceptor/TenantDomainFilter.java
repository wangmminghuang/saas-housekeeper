/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.interceptor;

import com.huawei.saashousekeeper.config.binding.SchemaBindingStrategy;
import com.huawei.saashousekeeper.constants.Constants;
import com.huawei.saashousekeeper.exception.RoutingException;
import com.huawei.saashousekeeper.properties.ResourcesExcluderProperties;
import com.huawei.saashousekeeper.properties.TenantProperties;

import lombok.extern.log4j.Log4j2;

import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.Ordered;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.servlet.HandlerExceptionResolver;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 租户标识过滤器
 *
 * @author lWX1128557
 * @since 2022-04-01
 */
@Log4j2
@RefreshScope
public class TenantDomainFilter implements Filter, Ordered {
    private final AntPathMatcher pathMatcher = new AntPathMatcher();

    @Autowired
    private SchemaBindingStrategy schemaAdapter;

    @Autowired
    private TenantProperties tenantProperties;

    @Autowired
    @Qualifier("handlerExceptionResolver")
    private HandlerExceptionResolver resolver;

    @Autowired
    private ResourcesExcluderProperties resourcesExcluderProperties;

    @Override
    public int getOrder() {
        return HIGHEST_PRECEDENCE;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
        throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String tenantDomain = request.getHeader(Constants.TENANT_DOMAIN);

        // MDC埋点
        MDC.put(Constants.TENANT_ID, tenantDomain);

        // 过滤资源
        String[] excludedUris = resourcesExcluderProperties.getExcludedUris();
        for (String uris : excludedUris) {
            if (pathMatcher.match(uris, request.getRequestURI())) {
                log.info("-->Tenant ID filter, filtering out url={}: " + request.getRequestURI());
                filterChain.doFilter(request, response);
                MDC.clear();
                return;
            }
        }

        try {
            if (Objects.isNull(schemaAdapter.getSchema(tenantDomain))) {
                throw new RoutingException("Routing failure,The corresponding database could not be found");
            }
            filterChain.doFilter(request, response);
        } catch (Exception e) {
            resolver.resolveException(request, response, null, e);
        } finally {
            MDC.clear();
        }
    }
}
