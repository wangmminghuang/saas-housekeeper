/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.constants;

/**
 * 负载均衡算法类型
 *
 * @author lWX1156935
 * @since 2022/4/22
 */
public enum LoadBalanceStrategyEnum {
    RANDOM("random");

    private String code;

    LoadBalanceStrategyEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
