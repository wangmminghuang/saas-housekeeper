import HttpRequest from './axios';
const axios = new HttpRequest('/api-admin');

export function queryFn(url: string, params: any) {
    return axios.request({
        url,
        method: params.method,
        data: params.data,
    });
}

/**
 * 获取验证码接口
 * @param params
 */
export function identifyCode(params: any) {
    return queryFn('tenant/identifyCode', params);
}

/**
 * 获取邮箱接口
 * @param params
 */
export function getEmailDto(params: any) {
    return queryFn('/tenant/getEmail', params);
}

/**
 * 创建用户信息/用户注册 POST
 * @param params
 */
export function registerApi(params: { method: string; data: {} }) {
    return queryFn('/tenant/tenant', params);
}
