
package com.huawei.housekeeper.service;

import com.huawei.housekeeper.result.Result;
import com.huawei.housekeeper.controller.request.SetPropertyDto;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 功能描述
 *
 * @author lWX1128557
 * @since 2022-03-24
 */
@Component
@FeignClient(value = "config-server")
public interface TenantSchemaService {
    @PostMapping(value = "/property/add")
    Result<Integer> addProperties(@RequestBody SetPropertyDto setPropertyDto);
}
