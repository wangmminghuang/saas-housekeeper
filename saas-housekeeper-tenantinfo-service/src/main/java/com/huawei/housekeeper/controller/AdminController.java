/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller;

import com.huawei.housekeeper.result.ListRes;
import com.huawei.housekeeper.result.Result;
import com.huawei.housekeeper.controller.request.AdminLoginDto;
import com.huawei.housekeeper.controller.request.PageQueryTenantDto;
import com.huawei.housekeeper.controller.request.UpdateTenantDto;
import com.huawei.housekeeper.controller.response.GetMigrationInformationVo;
import com.huawei.housekeeper.controller.response.GetTenantDetailVo;
import com.huawei.housekeeper.service.AdminContextService;
import com.huawei.housekeeper.service.TenantService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 管理员控制层
 *
 * @author lWX1128557
 * @since 2022-03-03
 */
@RestController
@RequestMapping(value = "/admin")
@Api(value = "管理员登录")
@Valid
public class AdminController {
    @Autowired
    private AdminContextService adminContextService;

    @Autowired
    private TenantService tenantService;

    @PostMapping(value = "login")
    @ApiOperation(value = "管理员登录")
    public Result<String> adminLogin(@RequestBody @Valid AdminLoginDto adminLoginDto) {
        return Result.createResult(adminContextService.adminLogin(adminLoginDto));
    }

    @PreAuthorize("hasAnyRole('ROLE_Admin') or hasAnyRole('ROLE_Temp')")
    @PostMapping(value = "tenants")
    @ApiOperation(value = "管理员查询租户列表")
    public Result<ListRes<GetTenantDetailVo>>
        getTenantDetails(@RequestBody @Valid PageQueryTenantDto pageQueryTenantDto) {
        return Result.createResult(tenantService.getTenantDetails(pageQueryTenantDto));
    }

    @PreAuthorize("hasAnyRole('ROLE_Admin')")
    @PutMapping(value = "tenant")
    @ApiOperation(value = "管理员更新租户状态")
    public Result<Integer> updateTenantStatus(@RequestBody @Valid UpdateTenantDto updateTenantDto) {
        return Result.createResult(tenantService.updateTenantStatus(updateTenantDto));
    }

    @PutMapping(value = "migration")
    @ApiOperation(value = "迁移所有租户数据库")
    public Result<GetMigrationInformationVo> migrationAllTenant() {
        return Result.createResult(tenantService.migrationAllTenant());
    }
}
