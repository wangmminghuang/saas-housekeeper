/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.converter;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.huawei.housekeeper.controller.request.CreateUserDto;
import com.huawei.housekeeper.controller.response.GetTenantUserInfoVo;
import com.huawei.housekeeper.controller.response.GetUserInfoVo;
import com.huawei.housekeeper.controller.response.GetUserVo;
import com.huawei.housekeeper.controller.response.GetWorkUserInfoVo;
import com.huawei.housekeeper.dao.entity.User;

/**
 * 参数转换接口
 *
 * @author l84165417
 * @since 2022/1/27 10:38
 */
@Mapper(componentModel = "spring")
public interface UserConverter {
    UserConverter INSTANCE = Mappers.getMapper(UserConverter.class);

    /**
     * users 转换 getUserVos
     *
     * @param users 查询的用户
     * @return 返回的集合
     */
    List<GetUserVo> toGetUserVo(List<User> users);

    /**
     * 创建请求入参 转 user
     *
     * @param createUserDto
     * @return user
     */
    User createUserToUser(CreateUserDto createUserDto);

    /**
     * users 转换 getUserInfoVos
     *
     * @param users 查询的用户详情
     * @return 返回的集合
     */
    GetUserInfoVo toGetUserInfoVo(User users);

    /**
     * users 转换 getTenantUserInfoVo
     *
     * @param users 租户查用户详情
     * @return 返回的集合
     */
    List<GetTenantUserInfoVo> toGetTenantUserInfoVo(List<User> users);

    /**
     * users 转换 getUserInfoVos
     *
     * @param user 查询工人详情
     * @return 返回的集合
     */
    GetWorkUserInfoVo toGetWorkUserInfoVo(User user);
}
