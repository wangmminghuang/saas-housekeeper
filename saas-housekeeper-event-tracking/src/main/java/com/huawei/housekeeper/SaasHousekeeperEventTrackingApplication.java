/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper;

import com.huawei.housekeeper.config.AutoConfiguration;
import com.huawei.housekeeper.utils.MessageServiceUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

/**
 * 主启动类
 *
 * @author lWX1128557
 * @since 2022-07-20
 */

@SpringBootApplication(scanBasePackages = {"com.huawei.**"}, exclude = { DataSourceAutoConfiguration.class})
@ComponentScan(basePackages = "com.huawei.**",
        excludeFilters = {@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {MessageServiceUtil.class, AutoConfiguration.class})})
public class SaasHousekeeperEventTrackingApplication {

    public static void main(String[] args) {
        SpringApplication.run(SaasHousekeeperEventTrackingApplication.class, args);
    }

}
